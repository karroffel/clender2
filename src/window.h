#ifndef WINDOW_H
#define WINDOW_H

#include <stdbool.h>
#include <stdint.h>

bool window_init(char *title, int width, int height);

void window_destroy();


void window_display(int width, int height, uint8_t *data); 




#endif
