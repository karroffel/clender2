#include "window.h"

#include <SDL2/SDL.h>

static SDL_Window *window;
static SDL_Surface *surface;


bool window_init(char *title, int width, int height)
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		return false;
	}

	
	window = SDL_CreateWindow(title,
	                          SDL_WINDOWPOS_UNDEFINED,
	                          SDL_WINDOWPOS_UNDEFINED,
	                          width,
	                          height,
	                          SDL_WINDOW_SHOWN);
	surface = SDL_GetWindowSurface(window);



	SDL_FillRect(surface, 0, 0);
	SDL_UpdateWindowSurface(window);

	return true;
}

void window_destroy()
{
	SDL_DestroyWindow(window);
	SDL_Quit();
}

void window_display(int width, int height, uint8_t *data)
{

	SDL_Surface *buf = SDL_CreateRGBSurfaceWithFormatFrom(data,
	                                                      width,
	                                                      height,
	                                                      32,
	                                                      4 * width,
	                                                      SDL_PIXELFORMAT_RGBA32);
	SDL_BlitScaled(buf, NULL, surface, NULL);
	SDL_UpdateWindowSurface(window);
	SDL_FreeSurface(buf);
}
