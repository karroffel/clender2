#include <stdio.h>

#include <CL/cl.h>

#include "utils.h"
#include "window.h"

#include "utils.c"
#include "window.c"


cl_float vertex_buffer_data[] = {
	-0.5, -0.5, +0.0,
	+0.5, -0.5, +0.0,
	+0.0, +0.5, +0.0
};


int main(int argc, char *argv[argc])
{
	int output_width = 1280;
	int output_height = 720;

	if (!window_init("clender2", output_width, output_height)) {
		exit(-1);
	}
	
	
	cl_platform_id platform;
	cl_int err = clGetPlatformIDs(1, &platform, NULL);
	CL_ERR_ABORT(err != CL_SUCCESS, "Can't find OpenCL platform");

	
	cl_uint devices_num;
	err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, &devices_num);
	CL_ERR_ABORT(err != CL_SUCCESS, "Can't query devices");

	cl_device_id devices[devices_num];
	clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, devices_num, devices, &devices_num);
	CL_ERR_ABORT(err != CL_SUCCESS, "Can't query devices");


	cl_device_id device = devices[0]; // NOTE: just using the first for now.


	// create context and command queue

	cl_context_properties properties[] = {
		CL_CONTEXT_PLATFORM, (cl_context_properties) platform,
		0
	};
	cl_context context;
	context = clCreateContext(properties,
	                          1,
	                          &device,
	                          0,
	                          0,
	                          &err);
	CL_ERR_ABORT(err != CL_SUCCESS, "Can't create context");
	
	cl_command_queue command_queue;
	command_queue = clCreateCommandQueue(context,
	                                     device,
	                                     0,
	                                     &err);
	
	// create buffers
	cl_image_format color_buffer_fmt = { CL_RGBA, CL_UNSIGNED_INT8 };
	const cl_image_desc color_buffer_desc = {
		CL_MEM_OBJECT_IMAGE2D,
		output_width,
		output_height,
		0,
		1,
		0,
		0,
		0,
		0,
		0
	};
	cl_mem color_buffer;
	color_buffer = clCreateImage(context,
	                             CL_MEM_WRITE_ONLY,
	                             &color_buffer_fmt,
	                             &color_buffer_desc,
	                             0,
	                             &err);
	CL_ERR_ABORT(err != CL_SUCCESS, "Can't create color buffer");


	// create an attribute buffer
	size_t attrib_buffer_size = sizeof(vertex_buffer_data); // TODO: dynamic size

	cl_mem attrib_buffer;
	attrib_buffer = clCreateBuffer(context,
	                               CL_MEM_READ_ONLY,
	                               attrib_buffer_size,
	                               0,
	                               &err);
	CL_ERR_ABORT(err != CL_SUCCESS, "Can't create attribute buffer");
	
	// write data into the buffer
	cl_event event_attrib_buffer_write;
	err = clEnqueueWriteBuffer(command_queue,
  	                           attrib_buffer,
  	                           CL_FALSE,
  	                           0,
  	                           sizeof(vertex_buffer_data),
  	                           vertex_buffer_data,
  	                           0,
  	                           0,
  	                           &event_attrib_buffer_write);

  	clWaitForEvents(1, &event_attrib_buffer_write);


	// create a program.

	size_t len;
	read_file("kernels/test.cl", &len, 0);

	char *contents = malloc(len);
	read_file("kernels/test.cl", &len, contents);
	
	
	cl_program program;
	program = clCreateProgramWithSource(context,
	                                    1,
	                                    (const char **)&contents,
	                                    &len,
	                                    &err);
	CL_ERR_ABORT(err != CL_SUCCESS, "Can't create program");

	err = clBuildProgram(program,
	                     1,
	                     &device,
	                     "-Werror",
	                     0,
	                     0);
	if (err != CL_SUCCESS) {
		
		size_t log_length;
		clGetProgramBuildInfo(program,
		                      device,
		                      CL_PROGRAM_BUILD_LOG,
		                      0,
		                      0,
		                      &log_length);

		char buffer[log_length];
		clGetProgramBuildInfo(program,
		                      device,
		                      CL_PROGRAM_BUILD_LOG,
		                      log_length,
		                      buffer,
		                      &log_length);

		fprintf(stderr, "%s\n", buffer);
		goto error_program_build;
	}

	// create kernel object and set args
	
	cl_kernel kernel_test;
	kernel_test = clCreateKernel(program, "test", &err);

	if (err != CL_SUCCESS) {
		fprintf(stderr, "Kernel \"test\" not present");
		fprintf(stderr, "%d\n", err);
		goto error_kernel_name;
	}

	size_t test_n_size[2] = { output_width, output_height };

	err  = clSetKernelArg(kernel_test, 0, sizeof(cl_mem), &color_buffer);
	CL_ERR_ABORT(err != CL_SUCCESS, "Can't set argument 0");
	err |= clSetKernelArg(kernel_test, 1, sizeof(cl_int), &output_width);
	CL_ERR_ABORT(err != CL_SUCCESS, "Can't set argument 1");
	err |= clSetKernelArg(kernel_test, 2, sizeof(cl_int), &output_height);
	CL_ERR_ABORT(err != CL_SUCCESS, "Can't set argument 2");
	err |= clSetKernelArg(kernel_test, 3, sizeof(cl_mem), &attrib_buffer);
	CL_ERR_ABORT(err != CL_SUCCESS, "Can't set argument 3");

	if (err != CL_SUCCESS) {
		fprintf(stderr, "Can't set \"test\" argument");
		goto error_kernel_arg;
	}

	// execute the kernel
	cl_event event_execute_kernel;	
	err = clEnqueueNDRangeKernel(command_queue,
	                             kernel_test,
	                             2,
	                             0,
	                             test_n_size,
	                             0,
	                             0,
	                             0,
	                             &event_execute_kernel);
	if (err == CL_SUCCESS) {
		clWaitForEvents(1, &event_execute_kernel);
	} else {
		fprintf(stderr, "can't execute kernel\n");
		exit(-1);
	}
	// read back the contents from the out buffer
	
	uint8_t *color_buffer_buffer = malloc(output_width * output_height * 4);

	cl_event event_read_image;
	
	const size_t origin[3] = {0, 0, 0};
	const size_t region[3] = {output_width, output_height, 1};
	err = clEnqueueReadImage(command_queue,
	                         color_buffer,
	                         CL_FALSE,
	                         origin,
	                         region,
	                         0,
	                         0,
	                         color_buffer_buffer,
	                         0,
	                         0,
	                         &event_read_image);

	clWaitForEvents(1, &event_read_image);


	bool running = true;
	while (running) {
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_QUIT:
				running = false;
				continue;
			default: break;
			}
		}
		window_display(output_width, output_height, color_buffer_buffer);
	}

	free(color_buffer_buffer);



	clReleaseEvent(event_read_image);
	clReleaseEvent(event_execute_kernel);
error_kernel_arg:
error_kernel_name:	
	clReleaseProgram(program);	
	
error_program_build:
	clReleaseMemObject(attrib_buffer);
	clReleaseMemObject(color_buffer);

	clReleaseCommandQueue(command_queue);
	clReleaseContext(context);


	window_destroy();
	return 0;
}
