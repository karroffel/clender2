#include "utils.h"


bool read_file(char *path, size_t *buffer_len, char buffer[*buffer_len])
{
	FILE *f = fopen(path, "r");
	if (!f) {
		return false;
	}

	if (buffer && buffer_len) {
		// length and buffer given, now just fill the buffer
		fread(buffer, 1, *buffer_len, f);
		fclose(f);
		return true;
	} else if (buffer_len) {

		// no buffer given, find out length
		fseek(f, 0L, SEEK_END);
		long pos = ftell(f);
		
		fclose(f);
		*buffer_len = pos;
		return true;
	}
	return false;
}
