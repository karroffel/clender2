uint4 fragment(float x, float y)
{
	uint4 color = (uint4) (0, 0, 0, 255);
	color.x = pown((x + 1.0) / 2.0, 2) * 255;
	color.z = pown((y + 1.0) / 2.0, 2) * 255;
	return color;
}




kernel void test(write_only image2d_t color_buffer,
                 int render_width,
                 int render_height,
                 global float *vertex_buffer)
{
	int proc_x = get_global_id(0);
	int proc_y = get_global_id(1);

	// convert position to NDC
	float x = (float) proc_x / (float) render_width;
	float y = (float) proc_y / (float) render_height;
	x = (x * 2.0) - 1.0;
	y = (y * 2.0) - 1.0;


	uint4 color = fragment(x, y);
	write_imageui(color_buffer, (int2)(proc_x, proc_y), color);
}
