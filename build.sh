#!/bin/bash

pushd src
clang -g -std=c99 main.c -o ../bin/clender2 `sdl2-config --cflags` `sdl2-config --libs` -DCL_USE_DEPRECATED_OPENCL_1_2_APIS -lOpenCL
popd
