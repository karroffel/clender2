#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <stdlib.h>

#include <stdint.h>
#include <stdbool.h>

#define CL_ERR_ABORT(cond, msg) do { \
	if (cond) { \
		fprintf(stderr, "CL cond failed: %s -> %s\n", #cond, msg); \
		exit(-1); \
	} \
} while (0)


bool read_file(char *path, size_t *buffer_size, char *buffer);

#endif
